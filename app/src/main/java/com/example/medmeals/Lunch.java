package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class Lunch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lunch);

        ImageView ImgChapsuey = findViewById(R.id.ImgChapsuey);
        ImageView  ImgShrimp = findViewById(R.id.ImgShrimp);
        ImageView ImgCaesar = findViewById(R.id.ImgCaesar);
        ImageView  ImgTso = findViewById(R.id.ImgTso);

        ImgChapsuey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Lunch.this, Chapsuey.class);
                startActivity(intent1);
            }
        });
        ImgShrimp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Lunch.this, Shrimp_Fajitas.class);
                startActivity(intent1);
            }
        });
        ImgCaesar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Lunch.this, Caesar_Salad.class);
                startActivity(intent1);
            }
        });
        ImgTso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Lunch.this, Tsos_Chicken.class);
                startActivity(intent1);
            }
        });
    }
}