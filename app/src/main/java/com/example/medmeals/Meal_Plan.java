package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class Meal_Plan extends AppCompatActivity {

    EditText editTextDay1, editTextBreakfast1, editTextLunch1, editTextDinner1, editTextDay2, editTextBreakfast2, editTextLunch2, editTextDinner2;

    Button BtnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_plan);

        editTextDay1 = (EditText) findViewById(R.id.editTextDay1);
        editTextBreakfast1 = (EditText) findViewById(R.id.editTextBreakfast1);
        editTextLunch1 = (EditText) findViewById(R.id.editTextLunch1);
        editTextDinner1 = (EditText) findViewById(R.id.editTextDinner1);
        editTextDay2 = (EditText) findViewById(R.id.editTextDay2);
        editTextBreakfast2 = (EditText) findViewById(R.id.editTextBreakfast2);
        editTextLunch2 = (EditText) findViewById(R.id.editTextLunch2);
        editTextDinner2 = (EditText) findViewById(R.id.editTextDinner2);
        BtnSave = (Button) findViewById(R.id.BtnSave);

        BtnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String Day1 = editTextDay1.getText().toString();
                String Bf1 = editTextBreakfast1.getText().toString();
                String Lunch1 = editTextLunch1.getText().toString();
                String Dinner1 = editTextDinner1.getText().toString();
                String Day2 = editTextDay2.getText().toString();
                String Bf2 = editTextBreakfast2.getText().toString();
                String Lunch2 = editTextLunch2.getText().toString();
                String Dinner2 = editTextDinner2.getText().toString();


                Intent intent = new Intent(Meal_Plan.this, Display.class);
                intent.putExtra("Day1_key",Day1);
                intent.putExtra("Bf1_key",Bf1);
                intent.putExtra("Lunch1_key",Lunch1);
                intent.putExtra("Dinner_key",Dinner1);
                intent.putExtra("Day2_key",Day2);
                intent.putExtra("Bf2_key",Bf2);
                intent.putExtra("Lunch2_key",Lunch2);
                intent.putExtra("Dinner2_key",Dinner2);
                startActivity(intent);

            }
        });
    }
}