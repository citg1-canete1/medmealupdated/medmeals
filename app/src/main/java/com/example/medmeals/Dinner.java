package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class Dinner extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dinner);

        ImageView ImgPancit = findViewById(R.id.ImgPancit);
        ImageView ImgTandori = findViewById(R.id.ImgTandori);
        ImageView ImgBulgogi = findViewById(R.id.ImgBulgogi);
        ImageView Imgmiso = findViewById(R.id.Imgmiso);

        ImgPancit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Dinner.this, Pancit_Canton.class);
                startActivity(intent1);
            }
        });
        ImgTandori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Dinner.this, Tandori.class);
                startActivity(intent1);
            }
        });
        ImgBulgogi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Dinner.this, bulgogi.class);
                startActivity(intent1);
            }
        });
        Imgmiso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Dinner.this, Miso_Souo.class);
                startActivity(intent1);
            }
        });

    }
}