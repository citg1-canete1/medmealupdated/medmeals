package com.example.medmeals;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.activity.result.ActivityResultLauncher;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;


public class Scanner extends AppCompatActivity {

    Button btn_scan, regToHome, regToInfo, regToSched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        btn_scan = findViewById(R.id.btn_scan);
        regToInfo = findViewById(R.id.btn_scanToInfo);
        regToHome = findViewById(R.id.btn_scanToHome);
        regToSched = findViewById(R.id.btn_scanToSched);


        regToInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Scanner.this, MainInfo.class);
                startActivity(intent1);
            }
        });
        regToSched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(Scanner.this, Schedule.class);
                startActivity(intent3);
            }
        });
        regToHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Scanner.this, Home_Page.class);
                startActivity(intent1);
            }
        });
        btn_scan.setOnClickListener(v ->
        {
            scanCode();

        });
    }

    private void scanCode() {
        ScanOptions options = new ScanOptions();
        options.setPrompt("Volume up to flash on");
        options.setBeepEnabled(true);
        options.setOrientationLocked(true);
        options.setCaptureActivity(CaptureAct.class);
        barLauncher.launch(options);

    }

    ActivityResultLauncher<ScanOptions> barLauncher = registerForActivityResult(new ScanContract(), result ->
    {
        if (result.getContents() != null) {
            String scannedText = result.getContents();

            // Create an intent to open the scanned link
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(scannedText));
            startActivity(intent);

            // Show an alert dialog with the scanned link
            AlertDialog.Builder builder = new AlertDialog.Builder(Scanner.this);
            builder.setTitle("Result");
            builder.setMessage(scannedText);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    });
}
