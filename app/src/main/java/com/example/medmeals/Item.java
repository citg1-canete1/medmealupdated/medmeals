package com.example.medmeals;

public class Item {
    private String itemName;
    private int imageRes;

    public Item(String itemName, int imageRes) {
        this.itemName = itemName;
        this.imageRes = imageRes;
    }

    public String getItemName() {
        return itemName;
    }

    public int getImageRes() {
        return imageRes;
    }
}
