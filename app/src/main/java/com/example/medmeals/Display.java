package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class Display extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        EditText editTextDay1 = findViewById(R.id.editTextDay1);
        EditText editTextBreakfast1 = findViewById(R.id.editTextBreakfast1);
        EditText editTextLunch1 = findViewById(R.id.editTextLunch1);
        EditText editTextDinner1 = findViewById(R.id.editTextDinner1);
        EditText editTextDay2 = findViewById(R.id.editTextDay2);
        EditText editTextBreakfast2 = findViewById(R.id.editTextBreakfast2);
        EditText editTextLunch2 = findViewById(R.id.editTextLunch2);
        EditText editTextDinner2 = findViewById(R.id.editTextDinner2);

        Intent intent = getIntent();
        String day1Value = intent.getStringExtra("Day1_key");
        String breakfast1Value = intent.getStringExtra("Bf1_key");
        String lunch1Value = intent.getStringExtra("Lunch1_key");
        String day2Value = intent.getStringExtra("Day2_key");
        String dinner1Value = intent.getStringExtra("Dinner_key");
        String breakfast2Value = intent.getStringExtra("Bf2_key");
        String lunch2Value = intent.getStringExtra("Lunch2_key");
        String dinner2Value = intent.getStringExtra("Dinner2_key");

        editTextDay1.setText(day1Value);
        editTextBreakfast1.setText(breakfast1Value);
        editTextLunch1.setText(lunch1Value);
        editTextDinner1.setText(dinner1Value);
        editTextDay2.setText(day2Value);
        editTextBreakfast2.setText(breakfast2Value);
        editTextLunch2.setText(lunch2Value);
        editTextDinner2.setText(dinner2Value);
    }
}