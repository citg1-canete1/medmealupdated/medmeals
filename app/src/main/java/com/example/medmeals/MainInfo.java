package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.example.medmeals.databinding.ActivityMainInfoBinding;

import java.util.ArrayList;

public class MainInfo extends AppCompatActivity {
    Button regToHomePage, regToScan, regToSchedule;
    ActivityMainInfoBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        binding = ActivityMainInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        regToHomePage= findViewById(R.id.btn_homePage);
        regToScan= findViewById(R.id.btn_infotoscan);
        regToSchedule= findViewById(R.id.btn_infoToSched);



        int[] imageId = {R.drawable.med,R.drawable.med,R.drawable.med,R.drawable.med,R.drawable.med,
                R.drawable.med,R.drawable.med,R.drawable.med,R.drawable.med};
        String[] name = {"Medicine","Medicine1","Medicine2","Medicine3","Medicine4","Medicine5","Medicine6","Medicine7","Medicine8"};
        String[] lastMessage = {"P5.00","P5.00","P5.00","P5.00","P5.00",
                "P5.00","P5.00","P5.00","P5.00"};
        String[] lastmsgTime = {"Unilab","Unilab","Unilab","Unilab","Unilab",
                "Unilab","Unilab","Unilab","Unilab"};
        String[] phoneNo = {"Medicine Info...","Medicine Info...","Medicine Info...","Medicine Info...","Medicine Info...",
                "Medicine Info...","Medicine Info...","Medicine Info...","Medicine Info..."};
        String[] country = {"Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513",
                "Address: 120 Colon St, Cebu City, 6000 Cebu\n" +
                "Hours:Open Now\n" +
                "Phone:(032) 253 0513"};

        ArrayList<Med> medArrayList = new ArrayList<>();

        for(int i = 0;i< imageId.length;i++){

            Med med = new Med(name[i],lastMessage[i],lastmsgTime[i],phoneNo[i],country[i],imageId[i]);
            medArrayList.add(med);

        }


        ListAdapter listAdapter = new com.example.medmeals.ListAdapter(MainInfo.this,medArrayList);

        binding.listview.setAdapter(listAdapter);
        binding.listview.setClickable(true);
        binding.listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainInfo.this,MedActivity.class);
                i.putExtra("name",name[position]);
                i.putExtra("phone",phoneNo[position]);
                i.putExtra("country",country[position]);
                i.putExtra("imageid",imageId[position]);
                startActivity(i);

            }
        });
        regToHomePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainInfo.this, Home_Page.class);
                startActivity(intent);
            }
        });
        regToScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainInfo.this, Scanner.class);
                startActivity(intent);
            }
        });
        regToSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainInfo.this, Schedule.class);
                startActivity(intent);
            }
        });
    }
}