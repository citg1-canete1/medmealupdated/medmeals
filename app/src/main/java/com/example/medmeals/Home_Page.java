package com.example.medmeals;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class Home_Page extends AppCompatActivity {
    Button regToSearchBtn, regToMedInfo, regToScan, regToSchedule, BtnMeals;
    Button BtnMealPlan, BtnMaps, BtnLunch, BtnDinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        BtnMeals = findViewById(R.id.BtnMeals);
        BtnLunch = findViewById(R.id.BtnLunch);
        BtnDinner = findViewById(R.id.BtnDinner);
        BtnMealPlan = findViewById(R.id.BtnMealPlan);
        BtnMaps = findViewById(R.id.BtnMaps);

        regToSearchBtn = findViewById(R.id.btn_ToSearch);
        regToMedInfo = findViewById(R.id.btn_medinfo);
        regToScan = findViewById(R.id.btn_ToScan);
        regToSchedule = findViewById(R.id.btn_ToSchedule);


        BtnMeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, breakfast.class);
                startActivity(intent1);
            }
        });
        BtnLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, Lunch.class);
                startActivity(intent1);
            }
        });
        BtnDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, Dinner.class);
                startActivity(intent1);
            }
        });
        regToSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, Schedule.class);
                startActivity(intent1);
            }
        });
        BtnMealPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, Meal_Plan.class);
                startActivity(intent1);
            }
        });
        regToSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(Home_Page.this, Search.class);
                startActivity(intent1);
            }
        });
        regToScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(Home_Page.this, Scanner.class);
                startActivity(intent2);
            }
        });
        BtnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:10.316720, 123.890710"));
                startActivity(intent3);
            }
        });
        regToMedInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(Home_Page.this, MainInfo.class);
                startActivity(intent3);
            }
        });
    }
}