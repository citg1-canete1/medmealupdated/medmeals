package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class breakfast extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breakfast);

        ImageView ImgWrap = findViewById(R.id.ImgWrap);
        ImgWrap.setOnClickListener(this);

        ImageView ImgEggplant = findViewById(R.id.ImgEggplant);
        ImgEggplant.setOnClickListener(this);

        ImageView ImgToast = findViewById(R.id.ImgToast);
        ImgToast.setOnClickListener(this);

        ImageView ImgAroz = findViewById(R.id.ImgAroz);
        ImgAroz.setOnClickListener(this);

        ImageView ImgBfTacos = findViewById(R.id.ImgBfTacos);
        ImgBfTacos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ImgWrap:
                Intent intent = new Intent(breakfast.this, Egg_Wrap.class);
                startActivity(intent);
                break;
            case R.id.ImgEggplant:
                Intent intent1 = new Intent(breakfast.this, EggPlant.class);
                startActivity(intent1);
                break;
            case R.id.ImgToast:
                Intent intent2 = new Intent(breakfast.this, Avocad_Toast.class);
                startActivity(intent2);
                break;
            case R.id.ImgAroz:
                Intent intent3 = new Intent(breakfast.this, Aroz_Caldo.class);
                startActivity(intent3);
                break;
            case R.id.ImgBfTacos:
                Intent intent4 = new Intent(breakfast.this, BfTacos.class);
                startActivity(intent4);
                break;

        }
    }
}
