package com.example.medmeals;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class Search extends AppCompatActivity {

    private List<Item> itemList;
    private ItemAdapter itemAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        SearchView searchView = findViewById(R.id.searchView);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterList(newText);
                return true;
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        itemList = new ArrayList<>();


        itemList.add(new Item("Medicine", R.drawable.med));
        itemList.add(new Item("Medicine1", R.drawable.med));
        itemList.add(new Item("Medicine2", R.drawable.med));
        itemList.add(new Item("Medicine3", R.drawable.med));
        itemList.add(new Item("Medicine4", R.drawable.med));
        itemList.add(new Item("Medicine5", R.drawable.med));
        itemList.add(new Item("Medicine6", R.drawable.med));
        itemList.add(new Item("Medicine7", R.drawable.med));
        itemList.add(new Item("Medicine8", R.drawable.med));

        itemAdapter = new ItemAdapter(itemList);
        recyclerView.setAdapter(itemAdapter);
    }

    private void filterList(String Text) {
        List<Item> filteredList = new ArrayList<>();
        for (Item item : itemList) {
            if (item.getItemName().toLowerCase().contains(Text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        if (filteredList.isEmpty()) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        } else {
            itemAdapter.setFilteredList(filteredList);
        }
    }
}
